/*
 * Copyright (c) 2022 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-sentry-turret-mcb.
 *
 * aruw-sentry-turret-mcb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-sentry-turret-mcb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-sentry-turret-mcb.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "chassis_mcb_can_comm.hpp"

#include "tap/architecture/endianness_wrappers.hpp"

#include "modm/architecture/interface/can_message.hpp"

#include "drivers.hpp"

ChassisMcbCanComm::ChassisMcbCanComm(Drivers* drivers)
    : drivers(drivers),
      commandMsgBitmask(),
      chassisCommandHandler(
          drivers,
          CHASSIS_MCB_COMMAND_RX_CAN_ID,
          CHASSIS_IMU_CAN_BUS,
          this,
          &ChassisMcbCanComm::handleChassisCommandMessage),
      timeSyncronizationHandler(
          drivers,
          SYNC_RX_CAN_ID,
          CHASSIS_IMU_CAN_BUS,
          this,
          &ChassisMcbCanComm::handleTimeSynchronizationMessage)
{
}

void ChassisMcbCanComm::init()
{
    chassisCommandHandler.attachSelfToRxHandler();
    timeSyncronizationHandler.attachSelfToRxHandler();
}

void ChassisMcbCanComm::handleChassisCommandMessage(const modm::can::Message& message)
{
    commandMsgBitmask.value = message.data[0];
    chassisMcbConnectedTimeout.restart(DISCONNECT_TIMEOUT_PERIOD);
}

void ChassisMcbCanComm::handleTimeSynchronizationMessage(const modm::can::Message& message)
{
    uint32_t masterSentTime = *reinterpret_cast<const uint32_t*>(message.data);
    currProcessingSyncData.slaveReceiveResponseTimeUs = tap::arch::clock::getTimeMicroseconds();
    currProcessingSyncData.masterReceiveReqTimeUs = masterSentTime;
    currProcessingSyncData.masterResponseTimeUs = masterSentTime;

    // short circuit if too much time between request and response or if the request time was
    // somehow larger than the response time (i.e. clock wrapping)
    if (currProcessingSyncData.slaveReceiveResponseTimeUs < currProcessingSyncData.slaveReqTimeUs ||
        currProcessingSyncData.slaveReceiveResponseTimeUs - currProcessingSyncData.slaveReqTimeUs >
            1000 * SYNC_REQUEST_PERIOD / 2)
    {
        return;
    }

    lastestSyncData = currProcessingSyncData;
}

void ChassisMcbCanComm::sendSynchronizationRequest()
{
    if (timeSyncLoopTimeout.execute() && drivers->can.isReadyToSend(CHASSIS_IMU_CAN_BUS))
    {
        modm::can::Message message(SYNC_TX_CAN_ID, 0);
        message.setExtended(false);
        currProcessingSyncData.slaveReqTimeUs = tap::arch::clock::getTimeMicroseconds();
        drivers->can.sendMessage(CHASSIS_IMU_CAN_BUS, message);
    }
}

void ChassisMcbCanComm::sendTurretStatusData(tap::gpio::Digital::InputPin limitSwitchPin)
{
    if (drivers->can.isReadyToSend(CHASSIS_IMU_CAN_BUS))
    {
        bool limitSwitchDepressed = !drivers->digital.read(limitSwitchPin);

        modm::can::Message msg(TURRET_STATUS_TX_CAN_ID, 1);
        msg.setExtended(false);

        msg.data[0] = static_cast<uint8_t>(limitSwitchDepressed) & 0b1;

        drivers->can.sendMessage(CHASSIS_IMU_CAN_BUS, msg);
    }
}

void ChassisMcbCanComm::sendIMUData()
{
    using namespace tap::communication::sensors::imu::mpu6500;
    const Mpu6500::ImuState imuState = drivers->mpu6500.getImuState();

    if (getImuRecalibrationRequested())
    {
        drivers->mpu6500.requestCalibration();
    }

    if (imuState == Mpu6500::ImuState::IMU_CALIBRATING)
    {
        clearImuRecalibration();
    }

    if ((imuState == Mpu6500::ImuState::IMU_CALIBRATED ||
         imuState == Mpu6500::ImuState::IMU_NOT_CALIBRATED) &&
        drivers->can.isReadyToSend(CHASSIS_IMU_CAN_BUS))
    {
        drivers->leds.set(tap::gpio::Leds::Green, blinkCounter < 50);
        blinkCounter = (blinkCounter + 1) % 100;

        static constexpr float ANGLE_FIXED_POINT_PRECISION = 360.0f / UINT16_MAX;

        uint32_t turretIMUDataTime =
            getTimeRelativeToMasterMicroseconds(drivers->mpu6500.getPrevIMUDataReceivedTime());

        modm::can::Message yawMessage(YAW_TX_CAN_ID, 7);
        yawMessage.setExtended(false);
        AngleMessageData* yawAngleData = reinterpret_cast<AngleMessageData*>(yawMessage.data);
        yawAngleData->angleFixedPoint =
            (drivers->mpu6500.getYaw() - 180.0f) / ANGLE_FIXED_POINT_PRECISION;
        yawAngleData->angleAngularVelocityRaw =
            drivers->mpu6500.getGz() * Mpu6500::LSB_D_PER_S_TO_D_PER_S;
        yawAngleData->seq = imuDataSeq;
        yawAngleData->timestamp = (turretIMUDataTime >> 16) & 0xffff;

        drivers->can.sendMessage(CHASSIS_IMU_CAN_BUS, yawMessage);

        modm::can::Message pitchMessage(PITCH_TX_CAN_ID, 7);
        pitchMessage.setExtended(false);
        AngleMessageData* pitchAngleData = reinterpret_cast<AngleMessageData*>(pitchMessage.data);

        pitchAngleData->angleFixedPoint = drivers->mpu6500.getPitch() / ANGLE_FIXED_POINT_PRECISION;
        pitchAngleData->angleAngularVelocityRaw =
            drivers->mpu6500.getGy() * Mpu6500::LSB_D_PER_S_TO_D_PER_S;
        pitchAngleData->seq = imuDataSeq;
        pitchAngleData->timestamp = turretIMUDataTime & 0xffff;

        drivers->can.sendMessage(CHASSIS_IMU_CAN_BUS, pitchMessage);

        imuDataSeq++;
    }
}

ChassisMcbCanComm::ChassisMcbRxHandler::ChassisMcbRxHandler(
    Drivers* drivers,
    uint32_t id,
    tap::can::CanBus cB,
    ChassisMcbCanComm* msgHandler,
    CanCommListenerFunc funcToCall)
    : CanRxListener(drivers, id, cB),
      msgHandler(msgHandler),
      funcToCall(funcToCall)
{
}

void ChassisMcbCanComm::ChassisMcbRxHandler::processMessage(const modm::can::Message& message)
{
    (msgHandler->*funcToCall)(message);
}
