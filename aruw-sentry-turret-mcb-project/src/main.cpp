/*
 * Copyright (c) 2020-2021 Advanced Robotics at the University of Washington <robomstr@uw.edu>
 *
 * This file is part of aruw-sentry-turret-mcb.
 *
 * aruw-sentry-turret-mcb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * aruw-sentry-turret-mcb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with aruw-sentry-turret-mcb.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "tap/architecture/clock.hpp"
#include "tap/architecture/endianness_wrappers.hpp"
#include "tap/architecture/periodic_timer.hpp"
#include "tap/architecture/profiler.hpp"
#include "tap/board/board.hpp"

#include "modm/architecture/interface/can_message.hpp"
#include "modm/architecture/interface/delay.hpp"

#include "chassis_mcb_can_comm.hpp"
#include "drivers_singleton.hpp"

// For communicating with the MCB

ChassisMcbCanComm chassisMcbCanComm(DoNotUse_getDrivers());

// Place any sort of input/output initialization here. For example, place
// serial init stuff here.
static void initializeIo(tap::Drivers *drivers);

// Anything that you would like to be called place here. It will be called
// very frequently. Use PeriodicMilliTimers if you don't want something to be
// called as frequently.
static void updateIo(tap::Drivers *drivers);

static constexpr float SAMPLE_FREQUENCY = 1000.0f;
static constexpr float MAHONY_KP = 0.1f;

static void processRawMpu6500DataMCBTilted(
    const uint8_t (&rxBuff)
        [tap::communication::sensors::imu::mpu6500::Mpu6500::ACC_GYRO_TEMPERATURE_BUFF_RX_SIZE],
    modm::Vector3f &accel,
    modm::Vector3f &gyro)
{
    accel.x = -LITTLE_ENDIAN_INT16_TO_FLOAT(rxBuff);
    accel.y = -LITTLE_ENDIAN_INT16_TO_FLOAT(rxBuff + 4);
    accel.z = -LITTLE_ENDIAN_INT16_TO_FLOAT(rxBuff + 2);

    gyro.x = -LITTLE_ENDIAN_INT16_TO_FLOAT(rxBuff + 8);
    gyro.y = -LITTLE_ENDIAN_INT16_TO_FLOAT(rxBuff + 12);
    gyro.z = -LITTLE_ENDIAN_INT16_TO_FLOAT(rxBuff + 10);
}

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    ::Drivers *drivers = DoNotUse_getDrivers();

    drivers->mpu6500.attachProcessRawMpu6500DataFn(processRawMpu6500DataMCBTilted);

    Board::initialize();
    initializeIo(drivers);

    tap::arch::PeriodicMilliTimer mainLoopTimeout(1000.0f / SAMPLE_FREQUENCY);

    while (1)
    {
        // do this as fast as you can
        PROFILE(drivers->profiler, updateIo, (drivers));

        if (mainLoopTimeout.execute())
        {
            PROFILE(drivers->profiler, drivers->mpu6500.periodicIMUUpdate, ());
            PROFILE(drivers->profiler, drivers->commandScheduler.run, ());
            PROFILE(drivers->profiler, chassisMcbCanComm.sendIMUData, ());
            PROFILE(drivers->profiler, chassisMcbCanComm.sendSynchronizationRequest, ());
        }

        modm::delay_us(10);
    }
    return 0;
}

static void initializeIo(tap::Drivers *drivers)
{
    drivers->can.initialize();
    drivers->leds.init();
    drivers->mpu6500.init(SAMPLE_FREQUENCY, MAHONY_KP, 0.0f);
    drivers->errorController.init();
    chassisMcbCanComm.init();
}

static void updateIo(tap::Drivers *drivers)
{
    drivers->canRxHandler.pollCanData();
    drivers->mpu6500.read();
}
